Vue.component('card', {
    props :["title","content"],
    template:  
        `<div class= "card" >
            <div class="card-body">
                <h3 class="card-title">
                 {{title}}
                </h3>
                <div class="card-text">
                    {{content}}
                </div>
                 <button @click="deleteCard" class="btn btn-sm btn-danger">Delete Me</button>
            </div>
        </div>
    `,
    methods : {
        deleteCard(){
            console.log("Delete button has been clicked");
            this.$emit('delete-article',this.title);
        }

    }
});


app = new Vue({
    el: "#app",
    data :{ 
        articleTitle : '' ,
        articleText : '',
        articles : [

            {
                title : 'Title 1',
                content : 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Labore doloribus natus dolorem, qui nemo pariatur libero. In, illum quis perferendis maxime eveniet quasi veritatis assumenda ea ratione, incidunt, molestias doloremque?'
            },
            {
                title : 'Title 2',
                content : 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Labore doloribus natus dolorem, qui nemo pariatur libero. In, illum quis perferendis maxime eveniet quasi veritatis assumenda ea ratione, incidunt, molestias doloremque?'
            },
            {
                title : 'Title 3',
                content : 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Labore doloribus natus dolorem, qui nemo pariatur libero. In, illum quis perferendis maxime eveniet quasi veritatis assumenda ea ratione, incidunt, molestias doloremque?'
            },
            {
                title : 'Title 4',
                content : 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Labore doloribus natus dolorem, qui nemo pariatur libero. In, illum quis perferendis maxime eveniet quasi veritatis assumenda ea ratione, incidunt, molestias doloremque?'
            },
            {
                title : 'Title 5',
                content : 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Labore doloribus natus dolorem, qui nemo pariatur libero. In, illum quis perferendis maxime eveniet quasi veritatis assumenda ea ratione, incidunt, molestias doloremque?'
            },
            {
                title : 'Title 6',
                content : 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Labore doloribus natus dolorem, qui nemo pariatur libero. In, illum quis perferendis maxime eveniet quasi veritatis assumenda ea ratione, incidunt, molestias doloremque?'
            },
        ]
    },
    computed : {

        disabledButton(){
            return !this.articleText || !this.articleTitle;
        }


    },
    methods : {
        
        addArticle(){
            this.articles.unshift({title : this.articleTitle , content : this.articleText});
            this.articleTitle ='';
            this.articleText = ''; 
        },
        removeArticle(title){

            this.articles = this.articles.filter(article => article.title !==title);
            // for (var i = 0 ; i < this.articles.length ; i++){
            //     var article = this.articles[i];
            //     if(article.title == title){
            //         console.log("Found element");
            //         this.articles.splice(i,1);
            //         return;
            //     }
            // }

            // console.log("Element not found");


        }
    }
});